package com.sap.client.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.sap.service.cloud.factory.CloudConfiguration;
import com.sap.service.cloud.factory.CloudFactoryService;
import com.sap.service.model.Service;

public class ServiceTest {

	private Service service;

	@Before
	public void setUp() throws Exception {

		CloudConfiguration.getInstance().setUrl(TestUtil.url);
		service = new Service();
		service.setDescription("fixing pipes");
		service.setName("Pipe Fix");
		service.setEstimatedDuration(45d);
		service.setSetFee(200d);
	}

	// @Test
	public void echo() {
		String test = "echo";

		String echo = CloudFactoryService.MOCKUP_SERVICE.echo("echo");
		assertEquals(test, echo);

	}

	@Test
	public void readAllTest() {
		List<Service> services = new ArrayList<Service>();

		services = CloudFactoryService.MOCKUP_SERVICE.readAll();
		assertNotNull(services);
		assertTrue(services.size() > 0);
		System.out.println(services);

	}

	@Test
	public void addTest() {

		long id = CloudFactoryService.MOCKUP_SERVICE.create(service);
		assertNotNull(id);
		assertTrue(id > 0);

	}

	@Test
	public void getTest() {

		Service returnedService = new Service();

		long id = CloudFactoryService.MOCKUP_SERVICE.create(service);
		assertNotNull(id);
		assertTrue(id > 0);
		System.out.println("Created ID: " + id);

		returnedService = CloudFactoryService.MOCKUP_SERVICE.read(id);
		System.out.println("Returned Services: " + returnedService.getName());

		assertNotNull(returnedService);
		assertTrue(returnedService.getId() > 0);
		long returnedId = returnedService.getId();
		assertEquals(id, returnedId);
	}

	@Test
	public void updateTest() {
		Service returnedService;

		long id = CloudFactoryService.MOCKUP_SERVICE.create(service);
		assertNotNull(id);
		assertTrue(id > 0);

		returnedService = CloudFactoryService.MOCKUP_SERVICE.read(id);
		assertNotNull(returnedService);
		assertTrue(returnedService.getId() > 0);
		long returnedId = returnedService.getId();
		assertEquals(id, returnedId);

		returnedService.setFeePerUnit(120d);
		long updatedId = CloudFactoryService.MOCKUP_SERVICE.update(returnedService);
		assertNotNull(updatedId);
		assertTrue(updatedId > 0);
	}

	@Test
	public void removeTest() {
		Service returnedService;

		// long id = CloudFactoryService.MOCKUP_SERVICE.create(service);
		// assertNotNull(id);
		// assertTrue(id > 0);
		//
		// returnedService = CloudFactoryService.MOCKUP_SERVICE.read(id);
		// assertNotNull(returnedService);
		// assertTrue(returnedService.getId() > 0);
		// long returnedId = returnedService.getId();
		// assertEquals(id, returnedId);
		//
		// returnedService.setFeePerUnit(120d);
		// long updatedId = CloudFactoryService.MOCKUP_SERVICE.update(returnedService);
		// assertNotNull(updatedId);
		// assertTrue(updatedId > 0);
//9 10 11 13 14 15
	
			Service removed = CloudFactoryService.MOCKUP_SERVICE.delete(15);
			assertNotNull(removed);
	

	}

}
