package com.sap.client.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import com.sap.service.cloud.rest.ServiceClient;
import com.sap.service.model.Service;
import com.sap.service.web.app.Application;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@ActiveProfiles(profiles = "service")
public class ServiceTest2 {


	ServiceClient serviceClient = new ServiceClient();
	private Service service;
	final String URL = "http://localhost:8080/web.app/";

	@Before
	public void setUp() throws Exception {

		service = new Service();
		service.setDescription("fixing pipes");
		service.setName("Pipe Fix");
		service.setEstimatedDuration(45d);
		service.setSetFee(200d);
	}

	@Test
	public void echoTest() {
		String echoString = "service echo";
		ResponseEntity<String> entity = new TestRestTemplate().getForEntity(URL + "echo/" + echoString, String.class);
		System.out.println("MESSAGE:" + entity.getBody());
		assertEquals(entity.getStatusCode(), HttpStatus.OK);
		assertTrue(entity.getBody().contains(echoString));
	}

	@Test
	public void addservice() {
		

		RestTemplate rest = new TestRestTemplate();

		ResponseEntity<Service> responseEntity = rest.postForEntity(URL + "create", service, Service.class);
		System.out.println("RESPONSE:" + responseEntity);
		System.out.println("MESSAGE:" + responseEntity.getBody());

		assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);

		Service serviceCreated = responseEntity.getBody();
		assertTrue(serviceCreated.getId() > 0);

	}

	@Test
	public void getservice() {
		

		RestTemplate rest = new TestRestTemplate();

		ResponseEntity<Service> responseEntity = rest.postForEntity(URL + "create", service, Service.class);

		assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);

		Service serviceCreated = responseEntity.getBody();
		assertTrue(serviceCreated.getId() > 0);

		ResponseEntity<Service> getResponseEntity = rest.getForEntity(URL + "read", Service.class, serviceCreated.getId());
		System.out.println("RESPONSE:" + getResponseEntity);
		System.out.println("MESSAGE:" + getResponseEntity.getBody());

		Service serviceReturned = getResponseEntity.getBody();
		assertEquals(getResponseEntity.getStatusCode(), HttpStatus.OK);
		assertTrue(serviceCreated.getId() == serviceReturned.getId());
		

	}

	@Test
	public void deleteservice() {


		RestTemplate rest = new TestRestTemplate();

		ResponseEntity<Service> responseEntity = rest.postForEntity(URL + "create", service, Service.class);

		assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);

		Service serviceCreated = responseEntity.getBody();
		assertTrue(serviceCreated.getId() > 0);
		
		ResponseEntity<Boolean> deleteResponseEntity = rest.exchange(URL + "/delete"+serviceCreated.getId(), HttpMethod.DELETE, null, Boolean.class);


		Boolean serviceDeleted = deleteResponseEntity.getBody();
		assertEquals(deleteResponseEntity.getStatusCode(), HttpStatus.OK);

		assertEquals(serviceDeleted, true);

	}
}
